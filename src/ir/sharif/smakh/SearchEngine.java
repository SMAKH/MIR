package ir.sharif.smakh;

import ir.sharif.smakh.utils.Pair;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static ir.sharif.smakh.utils.Utils.*;

public class SearchEngine {
    private Corpus corpus;
    private Index index;
    private Parser parser;

    public SearchEngine(Corpus corpus, Map<Integer, String> docs) throws IOException {
        this.corpus = corpus;
        index = new Index(docs);
        parser = index.getParser();
    }
    public void addDoc(String name, String doc) throws IOException {
        int docID = corpus.addDoc(name);
        index.addDoc(docID, doc);
    }
    public void removeDoc(String name, String doc) {
        Integer docID = corpus.getDocID(name);
        if (docID == null)
            return;
        index.removeDoc(docID, doc);
    }
    public Parser getParser() {
        return parser;
    }
    public Index getIndex() {
        return index;
    }

    public List<String> getReleventDocsByQuery(String query, boolean isBagOfWords, boolean isLNC_LTC) {
        List<Pair<Integer, List<String>>> docsByQuery = isBagOfWords? getDocsByBagOfWords(query): getDocsByPhrases(query);
        List<Pair<Double, String>> docsWithScore = new ArrayList<>();
        for (Pair<Integer, List<String>> docByQuery: docsByQuery){
            docsWithScore.add(new Pair<>(
                    calScore(docByQuery.one(), docByQuery.two(), isLNC_LTC),
                    corpus.getDocName(docByQuery.one())));
        }
        Pair<Double, String>[] docsWithScoreArr = docsWithScore.toArray(new Pair[docsWithScore.size()]);
        Arrays.sort(docsWithScoreArr);
        List<String> releventDocs = new ArrayList<>();
        for (Pair<Double, String> docWithScore: docsWithScoreArr)
            releventDocs.add(docWithScore.two());
        return releventDocs;
    }
    private List<Pair<Integer, List<String>>> getDocsByPhrases(String query) {
        List<String> words = parser.parseText(query);
        Pair<List<String>, List<String>> splittedQuery = splitPhrases(query);

        List<Integer> docs = null;
        StringBuilder allNonPhrase = new StringBuilder();
        for (String nonPhrase : splittedQuery.two())
            allNonPhrase.append(nonPhrase);
        if (allNonPhrase.toString().length() > 1) {
            List<Integer> finalDocs = new ArrayList<>();
            getDocsByBagOfWords(allNonPhrase.toString()).forEach(doc ->
                    finalDocs.add(doc.one()));
            docs = finalDocs;
        }
        for (String phrase : splittedQuery.one()) {
            List<Integer> phraseDocs = index.getDocsWithPhrase(parser.parseText(phrase));
            if (docs == null) {
                docs = phraseDocs;
                continue;
            }
            List<Integer> oldDocs = docs;
            docs = intersect(oldDocs, phraseDocs);
        }
        List<Pair<Integer, List<String>>> docsOfQuery = new ArrayList<>();
        if (docs != null)
            docs.forEach(doc -> docsOfQuery.add(new Pair<>(doc, words)));
        return docsOfQuery;
    }

    private List<Pair<Integer, List<String>>> getDocsByBagOfWords(String query) {
        List<String> words = parser.tokenize(query);
        List<List<String>> possibleBags = new ArrayList<>();
        for (String word: words) {
            if (word.startsWith("*") || word.endsWith("*")) {
                List<List<String>> oldBags = possibleBags;
                possibleBags = new ArrayList<>();
                for (String wildCardWord: index.getWildCardWords(word)) {
                    for (List<String> oldBag: oldBags) {
                        ArrayList<String> newBag = new ArrayList<>(oldBag);
                        newBag.add(wildCardWord);
                        possibleBags.add(newBag);
                    }
                }
            }
        }
        List<Pair<Integer, List<String>>> docsOfBagOfWords = new ArrayList<>();
        for (List<String> bagOfWords: possibleBags) {
            List<Integer> docs = null;
            for (int p = 0; p < bagOfWords.size(); p++) {
                String word = bagOfWords.get(p);
                List<Integer> wordDocs = index.getWordDocs(parser.parseToken(word));
                if (docs == null) {
                    docs = wordDocs;
                    continue;
                }
                List<Integer> oldDocs = docs;
                docs = intersect(oldDocs, wordDocs);
            }
            for (Integer docID: docs)
                docsOfBagOfWords.add(new Pair<>(docID, bagOfWords));
        }
        return docsOfBagOfWords;
    }

    private double calScore (Integer docID, List<String> queryWords, boolean isLNC_LTC) {
        List<Integer> tf = new ArrayList<>();
        List<Integer> df = new ArrayList<>();
        queryWords.forEach(word -> {
            tf.add(index.getWordDocFreq(word, docID));
            df.add(index.getWordDocs(word).size());
        });
        return isLNC_LTC? calScoreLNC_LTC(tf, df): calScoreLNN_LTN(tf, df);
    }
    private double calScoreLNN_LTN(List<Integer> tf, List<Integer> df) {
        Double tf_idf = 0.0, docNormal2 = 1.0, queryNormal2 = 1.0;
        for (int i = 0; i < tf.size(); i++) {
            tf_idf += (1 + Math.log(tf.get(i))) * (1 + Math.log(1.0)) * Math.log(tf.size() / df.get(i));
        }
        return tf_idf / (Math.sqrt(docNormal2) * Math.sqrt(queryNormal2));
    }
    private double calScoreLNC_LTC(List<Integer> tf, List<Integer> df) {
        Double tf_idf = 0.0, docNormal2 = 0.0, queryNormal2 = Math.sqrt(tf.size());
        for (int i = 0; i < tf.size(); i++) {
            tf_idf += (1 + Math.log(tf.get(i))) * (1 + Math.log(1.0)) * Math.log(tf.size() / df.get(i));
            docNormal2 += Math.pow(tf.get(i), 2.0);
        }
        return tf_idf / (Math.sqrt(docNormal2) * Math.sqrt(queryNormal2));
    }
    public Corpus getCorpus() {
        return corpus;
    }
}
