package ir.sharif.smakh;

import ir.sharif.smakh.utils.Pair;
import jhazm.tokenizer.WordTokenizer;
import nu.xom.IllegalDataException;

import java.io.IOException;
import java.util.*;

public class Index {
    private Map<String, HashSet<String>> bigramWords;
    private Map<String, List<Pair<Integer, List<Integer>>>> postingList;
    private Parser parser;

    public Index(Map<Integer, String> docs) throws IOException {
        parser = new Parser(docs.values());
        Map<Integer, List<String>> docWords = parser.parseTexts(docs);
        constuctBigrams(docs.values());
        constuctPostingList(docWords);
    }

    private void constuctPostingList(Map<Integer, List<String>> docWords) {
        postingList = new HashMap<>();
        for (Integer docID: docWords.keySet())
            addToPostingList(docID, docWords.get(docID));
    }

    private void constuctBigrams(Collection<String> docs) throws IOException {
        bigramWords = new HashMap<>();
        HashSet<String> words = new HashSet<>();
        for (String doc: docs)
            words.addAll(parser.tokenize(doc));
        addToBigrams(words);
    }

    private void addToBigrams(HashSet<String> words) {
        for (String word: words) {
            String changedWord = word + '$';
            String bigram = "$$";
            for (int i = 0; i < changedWord.length(); i++) {
                bigram = String.valueOf(bigram.charAt(1)) + String.valueOf(changedWord.charAt(i));
                if (!bigramWords.containsKey(bigram))
                    bigramWords.put(bigram, new HashSet<>());
                bigramWords.get(bigram).add(word);
            }
        }
    }
    private void addToPostingList(int docID, List<String> words) {
        for (int i = 0; i < words.size(); i++) {
            String word = words.get(i);
            if (!postingList.containsKey(word))
                postingList.put(word, new ArrayList<>());
            List<Pair<Integer, List<Integer>>> wordDocs = postingList.get(word);
            if (wordDocs.isEmpty() || wordDocs.get(wordDocs.size() - 1).one() < docID)
                wordDocs.add(new Pair<>(docID, new ArrayList<>()));
            wordDocs.get(wordDocs.size() - 1).two().add(i);
        }
    }
    private void removeFromPostingList(int docID, List<String> words) {
        for (int i = 0; i < words.size(); i++) {
            String word = words.get(i);
            if (postingList.containsKey(word)) {
                List<Pair<Integer, List<Integer>>> wordDocs = postingList.get(word);
                for (Pair<Integer, List<Integer>> wordDoc: wordDocs)
                    if (wordDoc.one().equals(docID)) {
                        wordDocs.remove(wordDoc);
                        break;
                    }
            }
        }
    }
    public void addDoc(int docID, String text) throws IOException {
        addToBigrams(new HashSet<>(parser.tokenize(text)));
        addToPostingList(docID, parser.parseText(text));
    }
    public void removeDoc(int docID, String text) {
        List<String> words = parser.parseText(text);
        removeFromPostingList(docID, words);
    }
    public Map<Integer, Integer> getWordDocsAndTDF(String word) {
        Map<Integer, Integer> docs = new HashMap<>();
        for (Pair<Integer, List<Integer>> wordDoc: postingList.get(word))
            docs.put(wordDoc.one(), wordDoc.two().size());
        return docs;
    }
    public List<Pair<Integer, List<Integer>>> getWordDocsAndPos(String word) {
        if (postingList.containsKey(word))
            return postingList.get(word);
        return new ArrayList<>();
    }
    public List<Integer> getWordDocs(String word) {
        List<Integer> wordDocs = new ArrayList<>();
        for (Pair<Integer, List<Integer>> wordDoc: getWordDocsAndPos(word))
            wordDocs.add(wordDoc.one());
        return wordDocs;
    }
    public Integer getWordDocFreq(String word, int docID) {
        for (Pair<Integer, List<Integer>> doc: getWordDocsAndPos(word))
            if (doc.one() == docID)
                return doc.two().size();
        return 0;
    }

    public List<String> getWildCardWords(String wildCard) {
        HashSet<String> words;
        Boolean startsWithStar = wildCard.startsWith("*");
        if (startsWithStar) {
            words = new HashSet<>(bigramWords.get(String.valueOf(wildCard.charAt(wildCard.length() - 1)) + "$"));
            wildCard = wildCard.substring(1, wildCard.length() - 1);
        }
        else {
            words = new HashSet<>(bigramWords.get("$" + String.valueOf(wildCard.charAt(0))));
            wildCard = wildCard.substring(0, wildCard.length() - 1);
        }
        for (int i = 0; i < wildCard.length() - 1; i++) {
            HashSet<String> newWords = bigramWords.get(wildCard.substring(i, 2));
            HashSet<String> oldWords = words;
            words = new HashSet<>();
            for (String word: oldWords)
                if (newWords.contains(word))
                    words.add(word);
        }
        HashSet<String> oldWords = words;
        words = new HashSet<>();
        for (String word: oldWords)
            if ( (startsWithStar && word.endsWith(wildCard)) ||
                    (!startsWithStar && word.startsWith(wildCard)))
                words.add(word);
        return new ArrayList<>(words);
    }
    public List<Integer> getDocsWithPhrase(List<String> phrase) {
        if (phrase.isEmpty())
            throw new IllegalDataException("Phrase is empty");
        List<Pair<Integer, List<Integer>>> docs = null;
        for (int p = 0; p < phrase.size(); p++) {
            String word = phrase.get(p);
            List<Pair<Integer, List<Integer>>> wordDocs = postingList.get(word);
            if (docs == null) {
                docs = wordDocs;
                continue;
            }
            List<Pair<Integer, List<Integer>>> oldDocs = docs;
            docs = new ArrayList<>();
            int i = 0, j = 0;
            while (i < oldDocs.size() && j < wordDocs.size()) {
                if (oldDocs.get(i).one() < wordDocs.get(j).one())
                    ++i;
                else if (oldDocs.get(i).one() > wordDocs.get(j).one())
                    ++j;
                else {
                    List<Integer> positions = new ArrayList<>();
                    List<Integer> oldPos = oldDocs.get(i).two();
                    List<Integer> newPos = wordDocs.get(j).two();
                    int n = 0, m = 0;
                    while(n < oldPos.size() && m < newPos.size()) {
                        if (oldPos.get(n) + p < newPos.get(m))
                            ++n;
                        else if (oldPos.get(n) + p > newPos.get(m))
                            ++m;
                        else
                            positions.add(oldPos.get(n));
                    }
                    if (!positions.isEmpty())
                        docs.add(new Pair<>(oldDocs.get(i).one(), positions));
                }
            }
        }
        List<Integer> docIDs = new ArrayList<>();
        for (Pair<Integer, List<Integer>> wordDoc: docs)
            docIDs.add(wordDoc.one());
        return docIDs;
    }

    public Parser getParser() {
        return parser;
    }
}