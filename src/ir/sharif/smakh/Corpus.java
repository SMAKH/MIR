package ir.sharif.smakh;

import ir.sharif.smakh.utils.Pair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Corpus {
    private Map<String, Integer> docIDs = new HashMap<>();
    private ArrayList<String> docNames;

    public Corpus(ArrayList<String> docNames) {
        this.docNames = docNames;
        for (int i = 0; i < docNames.size(); i++)
            docIDs.put(docNames.get(i), i);
    }

    public String getDocName(int i) {
        return docNames.get(i);
    }
    public Integer getDocID(String docName) {
        if (docIDs.containsKey(docName))
            return docIDs.get(docName);
        return null;
    }

    public Integer addDoc(String name) {
        if (!docIDs.containsKey(name)) {
            docIDs.put(name, docIDs.size());
            docNames.add(name);
        }
        return docIDs.get(name);
    }
}
