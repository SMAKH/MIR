package ir.sharif.smakh.utils;

import java.util.ArrayList;
import java.util.List;

public class Utils {
    public static List<Integer> intersect(List<Integer> a, List<Integer> b) {
        List<Integer> ans = new ArrayList<>();
        int i = 0, j = 0;
        while (i < a.size() && j < b.size()) {
            if (a.get(i) < b.get(j))
                ++i;
            else if (a.get(i) > b.get(j))
                ++j;
            else
                ans.add(a.get(i));
        }
        return ans;
    }

    public static Pair<List<String>, List<String>> splitPhrases(String query) {
        List<String> phrases = new ArrayList<>();
        List<String> nonPhrases = new ArrayList<>();
        int firstNotProccessed = 0;
        Boolean waitingToEnd = false;
        for (int i = 0; i < query.length(); i++) {
            if (query.charAt(i) == '"') {
                if (waitingToEnd) {
                    phrases.add(query.substring(firstNotProccessed, i - firstNotProccessed));
                    waitingToEnd = false;
                    firstNotProccessed = i + 1;
                }
                else {
                    if (i - firstNotProccessed > 0)
                        nonPhrases.add(query.substring(firstNotProccessed, i - firstNotProccessed));
                    waitingToEnd = true;
                    firstNotProccessed = i + 1;
                }
            }
        }
        if (firstNotProccessed < query.length())
            nonPhrases.add(query.substring(firstNotProccessed, query.length() - firstNotProccessed));
        return new Pair<>(phrases, nonPhrases);
    }
}
