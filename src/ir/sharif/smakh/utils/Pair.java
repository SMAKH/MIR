package ir.sharif.smakh.utils;

import nu.xom.IllegalDataException;
import org.joda.time.IllegalInstantException;

public class Pair<K, V> implements Comparable{
    private K one;
    private V two;

    public Pair(K one, V two) {
        this.one = one;
        this.two = two;
    }

    public K one() {
        return one;
    }

    public void one(K one) {
        this.one = one;
    }

    public V two() {
        return two;
    }

    public void two(V two) {
        this.two = two;
    }

    @Override
    public int compareTo(Object o) {
        if (o.getClass() != this.getClass())
            throw new IllegalDataException("Compare a pair with a nonpair");
        Pair other = (Pair) o;
        if (other.one.getClass() != one.getClass() ||
                other.two.getClass() != two.getClass())
            throw new IllegalDataException("Compare a pairs with different data types");
        Comparable one = (Comparable) this.one;
        Comparable otherOne = (Comparable) this.one;
        Comparable two = (Comparable) this.one;
        Comparable othertwo = (Comparable) this.one;
        int firstCompare = one.compareTo(otherOne);
        if (firstCompare != 0)
            return firstCompare;
        return two.compareTo(othertwo);
    }
}