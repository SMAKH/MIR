package ir.sharif.smakh;

import jhazm.Normalizer;
import jhazm.Stemmer;
import jhazm.tokenizer.WordTokenizer;

import java.io.IOException;
import java.util.*;

import static ir.sharif.smakh.utils.Statics.STOPWORD;

public class Parser {
    private Normalizer normalizer = new Normalizer();
    private WordTokenizer tokenizer = new WordTokenizer();
    private Stemmer stemmer = new Stemmer();
    private HashSet<String> stopWords;
    private final int stopWordCount = 30;

    public Parser(Collection<String> semicorpus) throws IOException {
        StringBuilder text = new StringBuilder();
        for (String doc: semicorpus)
            text.append(doc);
        String normalizedText = normalizer.run(text.toString());
        List<String> tokens = tokenizer.tokenize(normalizedText);
        tokens = removePunctuations(tokens);
        HashMap<String, Integer> wordCount = new HashMap<>();
        for (String token: tokens) {
            if (wordCount.containsKey(token))
                wordCount.put(token, wordCount.get(token) + 1);
            wordCount.put(token, 1);
        }
        stopWords = new HashSet<String>(calculateStopWords(wordCount));
    }

    public List<String> tokenize(String text) {
        String normalizedText = normalizer.run(text);
        List<String> tokens = tokenizer.tokenize(normalizedText);
        tokens = removePunctuations(tokens);
        return tokens;
    }

    public String parseToken(String token) {
        if (stopWords.contains(token))
                return STOPWORD;
        return stemmer.stem(token);
    }

    public List<String> parseText(String text) {
        String normalizedText = normalizer.run(text);
        List<String> tokens = tokenizer.tokenize(normalizedText);
        tokens.replaceAll(s -> {
            if (stopWords.contains(s))
                return STOPWORD;
            return s;
        });
        List<String> stemmedTokens = new ArrayList<>();
        for (String token: tokens)
            stemmedTokens.add(stemmer.stem(token));
        return stemmedTokens;
    }

    public Map<Integer, List<String>> parseTexts(Map<Integer, String> docs) {
        Map<Integer, List<String>> docWords = new HashMap<>();
        for (Integer i: docs.keySet())
            docWords.put(i, parseText(docs.get(i)));
        return docWords;
    }

    private List<String> removePunctuations(List<String> tokens) {
        String[] puncs = "[/?:>)\"(<.-_=+&$#@!.،؟»«}{]".split("");
        List<String> res = new ArrayList<>();
        for (String token: tokens) {
            Boolean add = true;
            for (String punc: puncs)
                if (token.contains(punc)) {
                    add = false;
                    break;
                }
            if (add)
                res.add(token);
        }
        return res;
    }
    public HashSet<String> getStopWords() {
        return stopWords;
    }

    private List calculateStopWords(HashMap<String, Integer> wordCount) {
        ArrayList<String> stopWords = new ArrayList<String>();
        for (String word : wordCount.keySet()) {
            Integer count = wordCount.get(word);
            if (stopWords.size() < stopWordCount)
                stopWords.add(word);
            else if (wordCount.get(stopWords.get(stopWordCount - 1)) < count) {
                stopWords.remove(stopWordCount - 1);
                stopWords.add(word);
            }
            for (int index = stopWords.size() - 2; index > -1; --index) {
                if (wordCount.get(stopWords.get(index)) < count) {
                    stopWords.set(index + 1, stopWords.get(index));
                    stopWords.set(index, word);
                }
            }
        }
        return stopWords;
    }
}
