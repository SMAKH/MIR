package ir.sharif.smakh;

import ir.sharif.smakh.utils.Pair;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class Main {
    private static Corpus corpus;
    private static SearchEngine searchEngine;
    private static Scanner input = new Scanner(System.in);
    private static String docsFolderAddress = null;

    public static void main(String[] args) throws IOException {
        do {
            System.out.println("Enter command: \n" +
                    "0: Exit\n" +
                    "1: TextOperation Commands\n" +
                    "2: Indexing Commands\n" +
                    "3: Search Commands\n" +
                    "4: Benchmark Commands");
            int command = input.nextInt();
            try {
                switch (command) {
                    case 0:
                        return;
                    case 1:
                        textOperationCommands();
                        break;
                    case 2:
                        indexingCommands();
                        break;
                    case 3:
                        searchCommands();
                        break;
                    case 4:
                        benchmarkCommands();
                        break;
                    default:
                        System.out.println("Input not valid, Enter a number from 0 to 4");
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } while (true);
    }

    // TODO: complete
    private static void benchmarkCommands() {
        System.out.println("Not programmed yet!");
    }

    private static void searchCommands() {
        if (searchEngine == null) {
            System.out.println("SearchEngine is null and you want to search");
            return;
        }
        System.out.println("Enter search type: ranked/exact");
        Boolean isRanked = input.next().equals("ranked");
        input.nextLine();
        System.out.println("Enter search query");
        String query = input.nextLine();
        System.out.println("Enter scoring type: \n" +
                "0: lnn-ltn\n" +
                "1: lnc-ltc");
        Boolean isLNC_LTC = input.nextInt() == 1;
        List<String> releventDocsByQuery = searchEngine.getReleventDocsByQuery(query, isRanked, isLNC_LTC);
        System.out.println("Relevant Docs: \n");
        for (int i = 0; i < Math.min(20, releventDocsByQuery.size()); i++)
            System.out.println(releventDocsByQuery.get(i));
        System.out.println("-----------------------------");
        do {
            System.out.println("Enter docName to show and \"EXIT\" to return to the last menu");
            String command = input.next();
            if (command.equals("EXIT"))
                return;
            else
                try {
                    loadAndPrintDoc(command);
                } catch (IOException e) {
                    e.printStackTrace();
                }
        } while (true);
    }

    private static void loadAndPrintDoc(String command) throws IOException {
        System.out.println(loadDoc(docsFolderAddress + command));
    }

    private static void indexingCommands() throws IOException {
        do {
            System.out.println("Enter command: \n" +
                    "0: Back\n" +
                    "1: BuildIndexFromFolderOfDocs\n" +
                    "2: add doc by name\n" +
                    "3: remove doc by name\n" +
                    "4: export index\n" +
                    "5: import index\n" +
                    "6: print a word's docs and positions\n" +
                    "7: print all words of a wildcard");
            int command = input.nextInt();
            switch (command) {
                case 0:
                    return;
                case 1:
                    buildIndexFormFolderOfDocs();
                    break;
                case 2:
                    addDoc();
                    break;
                case 3:
                    removeDoc();
                    break;
                case 4:
                    exportIndex();
                    break;
                case 5:
                    importIndex();
                    break;
                case 6:
                    printWordDocAndPos();
                    break;
                case 7:
                    printWildCardWords();
                    break;
                default:
                    System.out.println("Input not valid, Enter a number from 0 to 7");
                    break;
            }
        } while (true);
    }

    private static void printWildCardWords() {
        if (searchEngine == null) {
            System.out.println("SearchEngine is null and you want to find words matching a wildcard");
            return;
        }
        System.out.println("Input the wildcard, *artingWithStar or endingInSt*");
        String wildCard = input.next();
        System.out.println("Words matching " + wildCard + " :");
        for (String word : searchEngine.getIndex().getWildCardWords(wildCard))
            System.out.print(word + ", ");
        System.out.println("\n------------------------------");
    }

    private static void printWordDocAndPos() {
        if (searchEngine == null) {
            System.out.println("SearchEngine is null and you want to print a word's docs");
            return;
        }
        System.out.println("Input word");
        String word = input.next();
        System.out.println("Word's docs: ");
        for (Pair<Integer, List<Integer>> wordDocs : searchEngine.getIndex().getWordDocsAndPos(word)) {
            System.out.println(searchEngine.getCorpus().getDocName(wordDocs.one()) + " : ");
            for (Integer pos: wordDocs.two())
                System.out.print(pos + ", ");
            System.out.println("");
        }
        System.out.println("-------------------------------");
    }

    // TODO: complete
    private static void importIndex() {
        System.out.println("Not programmed yet");
    }

    private static void exportIndex() {
        System.out.println("Not programmed yet");
    }

    private static void removeDoc() {
        if (searchEngine == null) {
            System.out.println("SearchEngine is null and you want to remove a doc");
            return;
        }
        System.out.println("Input doc's name\n");
        String docName = input.next();
        String doc = null;
        try {
            doc = loadDoc(docsFolderAddress + docName);
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
        searchEngine.removeDoc(docName, doc);
    }

    private static void addDoc() throws IOException {
        if (searchEngine == null) {
            System.out.println("SearchEngine is null and you want to add a doc");
            return;
        }
        System.out.println("Input doc's name\n");
        String docName = input.next();
        String doc = null;
        try {
            doc = loadDoc(docsFolderAddress + docName);
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
        searchEngine.addDoc(docName, doc);
    }

    private static String loadDoc(String docAddress) throws IOException {
        return readFile(docAddress, StandardCharsets.UTF_8);
    }
    static String readFile(String path, Charset encoding) throws IOException {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, encoding);
    }

    private static void textOperationCommands() {
        if (searchEngine == null || searchEngine.getParser() == null) {
            System.out.println("SearchEngine has not been constructed yet!");
            return;
        }
        Parser parser = searchEngine.getParser();
        do {
            System.out.println("Enter command: \n" +
                    "0: Back\n" +
                    "1: Parse a line\n" +
                    "2: Print stopwords");
            int command = input.nextInt();
            input.nextLine();
            switch (command) {
                case 0:
                    return;
                case 1:
                    System.out.println("Input your string in a line");
                    String line = input.nextLine();
                    System.out.println(parser.parseText(line));
                    break;
                case 2:
                    System.out.println("Stopwords: ");
                    for (String stopword: parser.getStopWords())
                        System.out.print(stopword + ", ");
                    System.out.println("");
                    break;
                default:
                    System.out.println("Input not valid, Enter a number from 0 to 2");
                    break;
            }
        } while (true);
    }

    private static void buildIndexFormFolderOfDocs() throws IOException {
        System.out.println("Enter corpus folder address");
        String corpusAddress = input.next();
        Map<String, String> docs = null;
        try {
            docs = inputDocs(corpusAddress);
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
        List<String> docNames = new ArrayList<>();
        docs.keySet().forEach(doc -> docNames.add(doc));
        corpus = new Corpus((ArrayList<String>) docNames);
        searchEngine = constructSearchEngine(docs, corpus);
    }

    private static SearchEngine constructSearchEngine(Map<String, String> docs, Corpus corpus) throws IOException {
        Map<Integer, String> docsByID = new HashMap<>();
        for (String docName : docs.keySet())
            docsByID.put(corpus.getDocID(docName), docs.get(docName));
        return new SearchEngine(corpus, docsByID);
    }

    private static Map<String, String> inputDocs(String corpusAddress) throws IOException {
        docsFolderAddress = corpusAddress;
        Map<String, String> docs = new HashMap<>();
        File folder = new File(docsFolderAddress);
        File[] listOfFiles = folder.listFiles();

        if (listOfFiles != null) {
            for (File file : listOfFiles) {
                if (file.isFile()) {
                    docs.put(file.getName(), loadDoc(docsFolderAddress + file.getName()));
                }
            }
        }
        else {
            throw new IOException();
        }
        return docs;
    }
}
